<?php
namespace App\Http\Controllers\API;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\User; 
use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Facades\Mail;
use Validator;
class UserController extends Controller 
{
    public $successStatus;

    public function __construct()
    {
        $this-> successStatus=200;
    }/** 
     * login api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function login(){ 
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){ 
            $user = Auth::user(); 
            $success['token'] =  $user->createToken('MyApp')-> accessToken; 
            return response()->json(['success' => $success], $this-> successStatus); 
        } 
        else{ 
            return response()->json(['error'=>'Unauthorised'], 401); 
        } 
    }
/** 
     * Register api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
public function register(Request $request) 
{ 
    $validator = Validator::make($request->all(), [ 
        'name' => 'required', 
        'email' => 'required|email', 
        'password' => 'required', 
        'c_password' => 'required|same:password', 
    ]);
    if ($validator->fails()) { 
        return response()->json(['error'=>$validator->errors()], 401);            
    }
    $input = $request->all(); 
    $input['password'] = bcrypt($input['password']); 
    $user = User::create($input); 
    $success['token'] =  $user->createToken('MyApp')-> accessToken; 
    $success['name'] =  $user->name;

    $data=array(
        'name'=> $user->name,
    );
    $emailTo =  $user->email;
    Mail::send('emails.register',$data, function($message) use ($emailTo){
        $message->from('eduardrussy@gmail.com','Welcome New User');
        $message->to($emailTo)->subject('Welcome to Fitpal');
    });
    /*$to = $input['email'];
    $subject = "My subject";
    $txt = "Hello ".$input['email']."! wolcome to Filpat Acount";
    $headers = "From: eduardrussy@gmail.com";

    mail($to,$subject,$txt,$headers);*/
    return response()->json(['success'=>$success], $this-> successStatus); 
}
/** 
     * details api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
public function details() 
{ 
    $user = Auth::user(); 
    return response()->json(['success' => $user], $this-> successStatus); 
} 
}