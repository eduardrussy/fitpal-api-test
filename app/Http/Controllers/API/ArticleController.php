<?php

namespace App\Http\Controllers\API;


use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\User;
use App\Article; 
use Illuminate\Support\Facades\Auth; 
use Validator;

class ArticleController extends Controller
{
    public $successStatus;

    public function __construct()
    {
        $this-> successStatus=200;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request) 
    { 
        $validator = Validator::make($request->all(), [ 
            'name' => 'required', 
            'description' => 'required', 
        ]);
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }
        $input = $request->all(); 
        
        $user = Auth::user();
        $article= new \App\Article;
        $article->name=$input['name'];
        $article->description=$input['description'];
        $article->user_id=$user->id;
        $article->save();
        $success['article'] =  $article; 
        $success['autor'] =  $user;
        return response()->json(['success' => $success], $this-> successStatus); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
       
        $article= new \App\Article;
        $articles=$article->join('users', 'users.id', '=', 'articles.user_id')
            ->select('users.name as autor','users.email', 'articles.*')
            ->get();
        return response()->json(['success' => $articles], $this-> successStatus); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function edit(Article $article)
    {
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Article $article)
    {
        $user = Auth::user();
        $new= $article;
        if ($new->user_id==$user->id){
            if($request->get('name')){
                $new->name=$request->get('name');
            }
            if($request->get('description')){
                $new->description=$request->get('description');
            }
            $new->save();
            $success['article'] =  $new; 
            $success['autor'] =  $user;
            return response()->json(['success' => $success], $this-> successStatus); 
        }else{
            return response()->json(['error' => 'This article is not autorize'], $this-> successStatus); 
        }

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $article)
    {
        $user = Auth::user();
        $new = $article;
        if ($new->user_id==$user->id){
            $new->delete();
            return response()->json(['success' => 'Article is delete'], $this-> successStatus); 
        }else{
            return response()->json(['error' => 'This article is not autorize'], $this-> successStatus); 
        }
    }
}
