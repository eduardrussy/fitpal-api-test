<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## API REST TEST PARA FITPAL UTILIZANDO PassPort PARA METODO DE AUTENTICACION Y TOKEN

Esta es un API RETS creada con el fin de autenticar y registrar usuarios, generando un token para la manipulacion de Articulos.

## QUE ES PASSPORT

Las API generalmente usan tokens para autenticar usuarios y no mantienen el estado de la sesión entre las solicitudes. Laravel hace que la autenticación de la API sea muy sencilla utilizando Laravel Passport, que proporciona una implementación completa del servidor OAuth2 para su aplicación Laravel en cuestión de minutos.

## Instalación Local

Para la instalacion local solo basta seguir estos pasos.

- **Descargar el Repositorio**
- **Tener instalado composer**
- Ejecutar el comando **composer update**
- **Modificar el archivo .env.example para configurar sus datos de conexion a la db**
- **Modificar el archivo .env.example para configurar la cuenta SMTP para el envio del mail**
- **Guardar el archivo con el nombre .env**
- Ejecutar el comando **php artisan migrate**
- Ejecutar el comando **php artisan passport:install**
- Ejecutar el comando **ejecutar php artisan serve**


## Registrar

http://127.0.0.1:8000/api/register

Metodo : POST

Parametros
- email
- password
- c_password
- name
- email

Respuesta 
- Token
- Name

## Login

http://127.0.0.1:8000/api/login

Metodo : POST

Parametros
- email
- password

Respuesta 
- Token

## Crear Articulo

En esta api tienes que establecer dos encabezados como se detalla a continuación:

'headers' => [ 
'Accept' => 'application / json', 
'Authorization' => 'Bearer'. $accessToken,

]

Por lo tanto, asegúrese de encabezado arriba, de lo contrario no puede obtener resultados exitosos.

http://127.0.0.1:8000/api/createArticle

Metodo : POST

Parametros
- name
- description

Respuesta 
- Article
- Autor

## Editar Articulo

En esta api tienes que establecer dos encabezados como se detalla a continuación:

'headers' => [ 
'Accept' => 'application / json', 
'Authorization' => 'Bearer'. $accessToken,

]

Por lo tanto, asegúrese de encabezado arriba, de lo contrario no puede obtener resultados exitosos.

http://127.0.0.1:8000/api/editArticle/id_articulo

Metodo : PATCH

Parametros
- name
- description

Respuesta 
- Article
- Autor

## Eliminar Articulo

En esta api tienes que establecer dos encabezados como se detalla a continuación:

'headers' => [ 
'Accept' => 'application / json', 
'Authorization' => 'Bearer'. $accessToken,

]

Por lo tanto, asegúrese de encabezado arriba, de lo contrario no puede obtener resultados exitosos.

http://127.0.0.1:8000/api/deleteArticle/id_articulo

Metodo : DELETE

Respuesta 
- success

## Traer Articulos

http://127.0.0.1:8000/api/getArticles

Metodo : GET

Respuesta 
- Listado de articulos con su autor.




